FROM python:3.7-stretch AS build-img
RUN apt-get update
RUN apt-get install -y --no-install-recommends build-essential gcc

# Some extra python env vars
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

# Setup the virtualenv
#RUN virtualenv /opt/virtenv
RUN python -m venv /opt/virtenv

# Make sure to use the virtualenv:
ENV PATH="/opt/virtenv/bin:$PATH"

COPY requirements.txt .
RUN pip install -r requirements.txt



FROM python:3.7-slim
COPY --from=build-img /opt/virtenv /opt/virtenv

# Make sure to use the virtualenv:
ENV PATH="/opt/virtenv/bin:$PATH"

# Set an environment variable with the directory
# where we'll be running the app
ENV APP /pyapp

# Create the directory and instruct Docker to operate
# from there from now on
RUN mkdir $APP
WORKDIR $APP

COPY . .
EXPOSE 5000
ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:5000", "wsgi:application"]
