#!/usr/bin/env bash

#======================================================================

set -e

function error() {
    echo "Error: $1"
    exit -1
}

[[ -n "$1" ]] || error "Missing the name of the python script you want to run!!!"

#======================================================================

echo "Parametres -debug-: "${@:2}""

py_virt_env_name="venv"

# create the virtualenv
virtualenv ${py_virt_env_name} --python=python3

readonly sourceFile="./${py_virt_env_name}/bin/activate"
source ${sourceFile}

# virtualenv is now active.

python --version

# Install requirements
pip install -r requirements.txt

# If you want to use gunicorn, pass "guni" as parameter
# Sample: sudo ./run_app.sh app guni
 
if [ "${1}-X" == "--guni-X" ];then
  gunicorn -b 0.0.0.0:5000 "${2}"
else
  python "$@"
fi

# leave the virtualenv
deactivate

